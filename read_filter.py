#!/usr/bin/python3

# Copyright (C) 2020 Brian J. Stucky
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from argparse import ArgumentParser, Action
import os.path
from alignlib.seq_filter import SeqFilter


argp = ArgumentParser(
    description='Filters DNA sequencing reads by comparing them to one or '
    'more target reference sequences and 0 or more contaminant reference '
    'sequences.'
)
argp.add_argument(
    '-t', '--target', required=True, type=str, action='append',
    help='Target reference sequences in FASTA  or FASTQ format.  Can be '
    'repeated to specify multiple target reference input files.'
)
argp.add_argument(
    '-c', '--contaminant', required=False, type=str, action='append',
    default=[], help='Contaminant reference sequences in FASTA or FASTQ '
    'format.  Can be repeated to specify multiple contaminant reference input '
    'files'
)
argp.add_argument(
    '-E', '--E_threshold', required=False, type=float, default=1e-12,
    help='The E-value threshold for identifying unknown contaminant DNA '
    '(default: 1e-12).'
)
argp.add_argument(
    '-o', '--output_folder', required=False, type=str, default='read_filter',
    help='The path to a folder for the output files.  Relative paths will be '
    'interpreted according to the location of the first input file.'
)
argp.add_argument(
    'reads_file', type=str, nargs='+',
    help='The path to a FASTQ or FASTA file of DNA sequencing reads.'
)

args = argp.parse_args()

def updateOutput(seqs_processed):
    print('\b' * len(updateOutput.last_cnt_str), end='')
    updateOutput.last_cnt_str = '{0:,}'.format(seqs_processed)
    print(updateOutput.last_cnt_str, end='', flush=True)
updateOutput.last_cnt_str = ''

# Set the output folder.
out_folder = os.path.join(
    os.path.dirname(args.reads_file[0]), args.output_folder
)
out_folder = os.path.normpath(out_folder)

seqfilter = SeqFilter(args.target, args.contaminant, out_folder)
seqfilter.registerObserver(updateOutput, 100)

for fpath in args.reads_file:
    print('Processing file {0}...'.format(os.path.basename(fpath)))

    print('  Sequences processed: 0', end='', flush=True)
    updateOutput.last_cnt_str = '0'

    seqfilter.filterSeqs(fpath, args.E_threshold)

    print()

