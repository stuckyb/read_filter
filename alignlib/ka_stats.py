# Copyright (C) 2020 Brian J. Stucky
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Implements Karlin-Altschul statistics for local alignments.

import math
from collections import namedtuple


# Define the default K-A stats parameters.  These values are for the default
# blastn alignment scoring scheme, with a match score of 1, a mismatch score of
# -2, and linear gap penalties with a cost of -2.5 per gap position.  Parameter
# values can be obtained from blastn's output files or the tables in
# blast_stat.c.
_LAMBDA = 1.28
_K = 0.46
_H = 0.85


# Define a struct-like datatype for storing K-A statistics results.
KAStats = namedtuple(
    'KAStats',
    [
        # Effective search space size.
        'esss',
        # E-value.
        'E'
    ]
)


def calcKAStats(score, m, n):
    """
    Calculates K-A statistical values using the default K-A parameters.
    """
    esss = calcESSS(m, n, _K, _H)
    E = SToE(_LAMBDA, _K, score, esss)

    return KAStats(esss=esss, E=E)

def EToS(Lambda, K, E, ss_size):
    """
    Given Karlin-Altschul statistical parameters and a search space size,
    converts an E-value to a raw alignment score.  See documentation for SToE
    for more details.
    """
    # Following the approach used by blast_stat.c from the BLAST package, set a
    # minimum value for E to avoid floating-point problems in the calculatoin
    # of S.  See BlastKarlinEtoS_simple() in blast_stat.c.
    E_min = 1.0e-297
    E = max(E, E_min)

    S = math.ceil(math.log((K * ss_size) / E) / Lambda)

    return S

def SToE(Lambda, K, S, ss_size):
    """
    Given Karlin-Altschul statistical parameters and a search space size,
    calculates the E-value for a raw alignment score.  The formula for the
    E-value is derived as the expected value of the distribution of the number
    of "random" alignments with scores exceeding S, which is approximated by a
    Poisson distribution.  See results in:
      * Karlin, Dembo, and Kawabata (1990, pg 575)
        https://www.jstor.org/stable/2242124
      * Karlin and Altschul (1990, pg 2265)
        https://dx.doi.org/10.1073%2Fpnas.87.6.2264
      * Altschul (1991, equation 2)
        https://doi.org/10.1016/0022-2836(91)90193-a
      * Altschul and Gish (1996, pg 462)
        https://doi.org/10.1016/S0076-6879(96)66029-7.
    See also BLAST_KarlinStoE_simple() in blast_stat.c in the BLAST sources.
    """
    E = K * ss_size * math.exp(Lambda * S * -1)

    return E

def calcESSS(m, n, K, H):
    """
    Calculates the effective search space size using a formula derived from
    equation 5 in Altschul and Gish (1996, pg 462)
    (https://doi.org/10.1016/S0076-6879(96)66029-7).  This method does not
    account for the number of sequences in the database and is generally less
    sophisticated than the approach currently used by blastn, but it generates
    E-values that are similar to those calculated by blastn.

    m (int): The length of the query sequence.
    n (int): The length of the reference sequence (i.e., database size).
    H: The relative entropy of the scoring system.
    """
    l = int(math.log(K * m * n) / H)

    if l >= m:
        l = m - 1
    if l >= n:
        l = n - 1

    esss = (m - l) * (n - l)

    return esss

