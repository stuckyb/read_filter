# Copyright (C) 2020 Brian J. Stucky
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os
import os.path
import csv
import hashlib
import subprocess
import select
import selectors
from collections import namedtuple
from .seq_io import testSeqFileFormat, seqFileReader, SeqWriter


# Define a struct-like datatype for returning query results.
QueryResult = namedtuple(
    'QueryResult',
    [
        # Query sequence ID.
        'query_seq_id',
        # Reference sequence ID.
        'ref_seq_id',
        # Whether the reference sequence is a target reference.
        'is_ref_target',
        # Alignment score.
        'score',
        # E-value of the alignment score.
        'E_value',
        # Length of the alignment.
        'length',
        # Number of matching positions in the alignment.
        'n_ident',
        # Alignment beginning position on query.
        'q_begin',
        # Alignment ending position on query.
        'q_end',
        # Alignment beginning position on reference.
        'ref_begin',
        # Alignment ending position on reference.
        'ref_end'
    ]
)


class RefSequenceDB:
    def __init__(self, target_refs, contam_refs):
        """
        target_refs: A list of file paths for target reference sequences.
        contam_refs: A list of file paths for contaminant reference sequences.
        """
        # Confirm that all reference sequence files actually exist.
        for ref_file in target_refs + contam_refs:
            if not(os.path.isfile(ref_file)):
                raise Exception(
                    'Reference sequence file could not be found: {0}.'.format(
                        ref_file
                    )
                )

        self.dbname = self._getDBName(target_refs, contam_refs)
        self.dbpath = os.path.join('.', self.dbname)

        if not(os.path.isdir(self.dbpath)):
            if os.path.exists(self.dbpath):
                raise Exception(
                    'A file with the name of the sequence database directory '
                    'already exists: {0}.'.format(self.dbpath)
                )

            self._createDB(self.dbname, self.dbpath, target_refs, contam_refs)

        self._loadDB(self.dbname, self.dbpath)

        self.pr = None
        self.read_chunks = []
        self.read_buff = ''

        self.res_handlers = []

    def registerResultHandler(self, callback):
        """
        Registers a callable to receive query results when they are ready.  The
        callable interface should be callable(result), where "result" is a
        QueryResult object.  Multiple result handlers may be registered.
        """
        self.res_handlers.append(callback)

    def _launchExternProc(self):
        if os.cpu_count() > 1:
            th_cnt = os.cpu_count() - 1
        else:
            th_cnt = 1

        # Launch blastn.  Informal testing indicates that if "--parse_seqids"
        # is not used when building the BLAST database, the output field
        # "stitle" will contain all text after ">" in the FASTA sequence
        # description lines.
        args = [
            'blastn', '-word_size', '11', '-evalue', '10', '-max_hsps', '1',
            '-max_target_seqs', '1', '-db', self.dbname,
            '-num_threads', str(th_cnt), '-outfmt',
            '6 qseqid stitle score evalue length nident qstart qend sstart send'
        ]
        #print(' '.join(args))

        self.pr = subprocess.Popen(
            args, stdin=subprocess.PIPE, stdout=subprocess.PIPE,
            universal_newlines=True, cwd=self.dbpath
        )
        self.read_chunks = []
        self.read_buff = ''

    def query(self, seq_id, seq):
        """
        Searches the sequences database for a local alignment match.  Query
        processing is done asynchronously, so results are reported via
        callbacks.
        """
        if self.pr is None:
            self._launchExternProc()

        # Prepare the buffer for writing to the child process.
        in_str = '>' + seq_id + '\n' + seq + '\n'
        in_bytes = in_str.encode(self.pr.stdin.encoding, self.pr.stdin.errors)
        in_str_view = memoryview(in_bytes)
        in_offset = 0

        self.pr.stdin.flush()

        # Use I/O multiplexing to send the new query sequence to and read any
        # available output from the child process.
        with selectors.DefaultSelector() as sel:
            sel.register(self.pr.stdin, selectors.EVENT_WRITE)
            sel.register(self.pr.stdout, selectors.EVENT_READ)

            while len(sel.get_map()) >  1:
                ready = sel.select()
                for key, events in ready:
                    if (key.events & selectors.EVENT_READ) != 0:
                        chunk = os.read(key.fd, 32768)
                        if chunk != b'':
                            self.read_chunks.append(chunk)
                    if (
                        (key.events & selectors.EVENT_WRITE) != 0 and
                        # Only handle writes to stdin if there is no data
                        # waiting in the stdout pipe.
                        len(ready) < 2
                    ):
                        chunk = in_str_view[in_offset:in_offset + select.PIPE_BUF]
                        try:
                            in_offset += os.write(key.fd, chunk)
                        except BrokenPipeError:
                            sel.unregister(key.fileobj)
                            key.fileobj.close()
                        else:
                            if in_offset >= len(in_bytes):
                                sel.unregister(key.fileobj)

        if len(self.read_chunks) > 0:
            self._processReadChunks()

    def _processReadChunks(self):
        """
        Processes new data chunks read from the child process and sends any
        complete results back to the client.  Results are passed to all
        registered results handlers.
        """
        # Convert the raw read chunks to a string and translate newlines.
        raw_data = b''.join(self.read_chunks)
        raw_data_str = raw_data.decode(
            self.pr.stdout.encoding, self.pr.stdout.errors
        )
        raw_data_str = raw_data_str.replace('\r\n', '\n').replace('\r', '\n')
        self.read_buff += raw_data_str

        self.read_chunks.clear()

        # Process each output row in the read buffer.  Because blastn prints a
        # '\n' at the end of each sequence record, including the last one, we
        # won't miss any output lines by searching for '\n' characters to
        # delimit output rows.
        while self.read_buff.find('\n') != -1:
            parts = self.read_buff.split('\n', maxsplit=1)
            self.read_buff = parts[1]

            row = parts[0].split('\t')

            ref_seq_id = row[1]
            if (
                (ref_seq_id not in self.seq_ids_target) and
                (ref_seq_id not in self.seq_ids_contam)
            ):
                raise Exception(
                    'The reference sequence ID for the alignment, "{0}", does '
                    'not match any known reference sequence '
                    'IDs.'.format(ref_seq_id)
                )

            is_ref_target = ref_seq_id in self.seq_ids_target

            qr = QueryResult(
                query_seq_id = row[0],
                ref_seq_id = ref_seq_id,
                is_ref_target = is_ref_target,
                score = float(row[2]),
                E_value = float(row[3]),
                length = int(row[4]),
                n_ident = int(row[5]),
                q_begin = int(row[6]),
                q_end = int(row[7]),
                ref_begin = int(row[8]),
                ref_end = int(row[9])
            )

            for res_handler in self.res_handlers:
             res_handler(qr)
        
    def inputComplete(self):
        """
        Indicates that all input sequences have been sent and waits until all
        sequence processing is complete.
        """
        self.pr.stdin.close()

        # Read all remaining output from the child process.
        with selectors.DefaultSelector() as sel:
            sel.register(self.pr.stdout, selectors.EVENT_READ)

            while len(sel.get_map()) >  0:
                ready = sel.select()
                for key, events in ready:
                    if key.events & selectors.EVENT_READ != 0:
                        chunk = os.read(key.fd, 32768)
                        if chunk == b'':
                            sel.unregister(key.fileobj)
                            key.fileobj.close()
                        else:
                            self.read_chunks.append(chunk)

        if len(self.read_chunks) > 0:
            self._processReadChunks()

        self.pr.wait()
        self.pr = None

    def _getDBName(self, target_refs, contam_refs):
        """
        Generates a unique name for the database using this set of reference
        sequence files, with the name based on the MD5 hash of the full
        sequence file paths, the file sizes, and the modification times.
        """
        # Get the complete file paths, file sizes, and modify timestamps.
        hashables = []
        for ref_file in target_refs + contam_refs:
            hashable = os.path.abspath(ref_file)
            finfo = os.stat(hashable)
            hashable += ':{0},{1}'.format(finfo.st_size, finfo.st_mtime)
            hashables.append(hashable)

        all_hashables = ';'.join(hashables)
        #print(all_hashables)

        hash_obj = hashlib.md5(all_hashables.encode())

        return 'db-' + hash_obj.hexdigest()

    def _createDB(self, dbname, dbpath, target_refs, contam_refs):
        os.mkdir(dbpath)

        # Create the sequence IDs index.
        print('Creating sequence IDs index...')
        with open(os.path.join(dbpath, 'seq_index.csv'), 'w') as fout:
            writer = csv.writer(fout)
            writer.writerow(('seq_id', 'is_target'))

            for ref_file in target_refs:
                print('  Reading sequence info from {0}...'.format(ref_file))
                for seq_id, seq, quals in seqFileReader(ref_file, True):
                    writer.writerow((seq_id, True))

            for ref_file in contam_refs:
                print('  Reading sequence info from {0}...'.format(ref_file))
                for seq_id, seq, quals in seqFileReader(ref_file, True):
                    writer.writerow((seq_id, False))

        # Build the BLAST DB.
        print('\nCreating BLAST database...')
        path_strs = []
        for ref_file in target_refs + contam_refs:
            path_strs.append(
                '"{0}"'.format(os.path.abspath(ref_file))
            )

        fpaths_str = ' '.join(path_strs)

        args = [
            'makeblastdb', '-dbtype', 'nucl', '-title', dbname,
            '-in', fpaths_str, '-out', dbname
        ]
        res = subprocess.run(args, cwd=dbpath, check=True)

    def _loadDB(self, dbname, dbpath):
        self.seq_ids_target = set()
        self.seq_ids_contam = set()

        with open(os.path.join(dbpath, 'seq_index.csv')) as fin:
            reader = csv.DictReader(fin)
            for row in reader:
                if row['is_target'] == 'True':
                    self.seq_ids_target.add(row['seq_id'])
                else:
                    self.seq_ids_contam.add(row['seq_id'])

