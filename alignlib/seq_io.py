# Copyright (C) 2020 Brian J. Stucky
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Sequence file format constants.
FORMAT_FASTA = 0
FORMAT_FASTQ = 1
FORMAT_UNKNOWN = -1


def testSeqFileFormat(fpath):
    """
    Attempts to determine the type of a sequence file by inspecting its
    contents.  Blank lines at the beginning of a file are ignored.
    """
    with open(fpath) as fin:
        line = ''
        for line in fin:
            line = line.strip()
            if line != '':
                break

    if line.startswith(';') or line.startswith('>'):
        return FORMAT_FASTA
    elif line.startswith('@'):
        return FORMAT_FASTQ
    else:
        return FORMAT_UNKNOWN

def FASTAReader(fpath, names_only=False):
    """
    A FASTA file reader.  Blank lines and lines beginning with a ';' are
    ignored.  If a sequence is larger than seq_max, only the first seq_max
    characters will be returned.  If echo_fh is provided, sequences larger than
    seq_max will be written to echo_fh in their entirety.

    echo_fh: A file handle open for writing.
    """
    seq = None
    seq_id = ''

    with open(fpath) as fin:
        for line in fin:
            line = line.strip()

            if line.startswith('>'):
                if seq is not None:
                    yield seq_id, seq, ''

                seq_id = line[1:]
                seq = ''
            elif not(line.startswith(';')):
                if not(names_only):
                    seq += line

    if seq != '' or seq_id != '':
        yield seq_id, seq, ''

def FASTQReader(fpath, names_only=False):
    """
    A simple FASTQ file reader.  Note that this implementation does NOT support
    blank lines or multi-line sequence or quality strings.
    """
    seq = ''
    seq_id = ''
    qual = ''

    with open(fpath) as fin:
        line = next(fin).strip()
        while line != '':
            if line.startswith('@'):
                seq_id = line[1:]
            else:
                raise Exception('Invalid FASTQ file: {0}'.format(fpath))

            seq = next(fin).strip()

            line = next(fin)
            if not(line.startswith('+')):
                raise Exception('Invalid FASTQ file: {0}'.format(fpath))

            qual = next(fin).strip()

            yield seq_id, seq, qual

            line = next(fin).strip()

def seqFileReader(fpath, names_only=False):
    """
    An iterable sequence file reader that supports FASTA and FASTQ files.
    """
    fformat = testSeqFileFormat(fpath)

    if fformat == FORMAT_FASTA:
        return FASTAReader(fpath, names_only)
    elif fformat == FORMAT_FASTQ:
        return FASTQReader(fpath, names_only)
    else:
        raise Exception('Unknown file format: {0}'.format(fpath))


class SeqWriter:
    def __init__(self, out, f_format):
        if f_format not in (FORMAT_FASTA, FORMAT_FASTQ):
            raise Exception('Unknown sequence file format.')

        self.out = out
        self.f_format = f_format

    def writeSeq(self, seq_id, seq, quals):
        if self.f_format == FORMAT_FASTA:
            self.out.write('>{0}\n'.format(seq_id))
            self.out.write(seq)
            self.out.write('\n')
        elif self.f_format == FORMAT_FASTQ:
            self.out.write('@{0}\n'.format(seq_id))
            self.out.write(seq)
            self.out.write('\n+\n')
            self.out.write(quals)
            self.out.write('\n')

