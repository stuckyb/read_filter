# Copyright (C) 2020 Brian J. Stucky
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os.path
import time
from contextlib import contextmanager, ExitStack
from collections import deque
from alignlib.seq_db import RefSequenceDB
from alignlib.seq_io import testSeqFileFormat, seqFileReader, SeqWriter


@contextmanager
def n_open(fpath, *args):
    """
    A context manager and wrapper for open() that works with None values for
    fpath.  This makes working with optional files cleaner.
    """
    if fpath is None:
        yield None
    else:
        f = open(fpath, *args)
        try:
            yield f
        finally:
            f.close()


class SeqFilter:
    def __init__(self, target_refs, contam_refs, output_dir):
        """
        target_refs: A list of file paths for target reference sequences.
        contam_refs: A list of file paths for contaminant reference sequences.
        """
        self.output_dir = os.path.abspath(output_dir)
        if not(os.path.isdir(self.output_dir)):
            if os.path.exists(self.output_dir):
                raise Exception(
                    'A file with the name of the output folder already exists.'
                )
            else:
                os.mkdir(self.output_dir)

        self.seqdb = RefSequenceDB(target_refs, contam_refs)
        self.seqdb.registerResultHandler(self._handleResult)

        self.obs = None
        self.update_interval = 0

        self.on_t_writer = None
        self.off_t_writer = None

        self.queries = deque()
        self.processed_cnt = 0
        self.on_t_cnt = 0
        self.off_t_cnt_contam = 0
        self.off_t_cnt_unknown = 0
        self.eval_threshold = 0.01

    def registerObserver(self, callback, update_interval):
        """
        Registers a callable to be notified as sequence processing progresses.
        The interface of "callback" should be callback(seqs_processed), where
        "seqs_processed" is the number of sequences that have been fully
        processed at the time of the notification.

        update_interval (int): The notification interval, in number of
            sequences.
        """
        self.obs = callback
        self.update_interval = update_interval

    def _handleResult(self, qres):
        blast_seq_id = qres.query_seq_id

        # Iterate through the queue of query sequences until we get to the
        # returned sequence.  Because blastn returns results in the order of
        # the query sequences, any sequences in the queue before the returned
        # sequence are sequences that did not generate a hit.  Also, if blastn
        # returns sequences in a different order, then eventually the loop will
        # be unable to find a returned sequence in the queue and the loop will
        # exhaust the queue, leading to an IndexError exception.
        while self.queries[0][0] != blast_seq_id:
            self.off_t_writer.writeSeq(
                self.queries[0][1], self.queries[0][2], self.queries[0][3]
            )
            self.queries.popleft()
            self.processed_cnt += 1
            self.off_t_cnt_unknown += 1

            if (
                self.obs is not None and
                self.processed_cnt % self.update_interval == 0
            ):
                self.obs(self.processed_cnt)

        seq_id, seq, quals = self.queries[0][1:]

        if qres.is_ref_target and qres.E_value < self.eval_threshold:
            self.on_t_cnt += 1
            self.on_t_writer.writeSeq(seq_id, seq, quals)
        else:
            if not(qres.is_ref_target):
                self.off_t_cnt_contam += 1
            else:
                self.off_t_cnt_unknown += 1
            self.off_t_writer.writeSeq(seq_id, seq, quals)

        self.processed_cnt += 1
        #print(self.processed_cnt)

        self.queries.popleft()

        if (
            self.obs is not None and
            self.processed_cnt % self.update_interval == 0
        ):
            self.obs(self.processed_cnt)

    def _cleanupQueriesQueue(self):
        # Any sequences remaining in the queries queue after sequence
        # processing are sequences that did not generate a hit.  Process each
        # accordingly.
        while len(self.queries) > 0:
            self.off_t_writer.writeSeq(
                self.queries[0][1], self.queries[0][2], self.queries[0][3]
            )
            self.queries.popleft()
            self.processed_cnt += 1
            self.off_t_cnt_unknown += 1

            if (
                self.obs is not None and
                self.processed_cnt % self.update_interval == 0
            ):
                self.obs(self.processed_cnt)

    def filterSeqs(self, seqfile, eval_threshold):
        if eval_threshold > 10.0:
            raise Exception('The E-value threshold cannot exceed 10.0.')

        self.eval_threshold = eval_threshold
        t_start = time.perf_counter()

        # Build the output file names.
        report_fpath = os.path.join(self.output_dir, 'summary.txt')
        ofname_base = os.path.join(
            self.output_dir, os.path.splitext(os.path.basename(seqfile))[0]
        )
        on_t_fpath = ofname_base + '-ON-TARGET.fastq'
        off_t_fpath = ofname_base + '-OFF-TARGET.fastq'

        # Empty the query sequences queue and stats counters.
        self.queries.clear()
        self.processed_cnt = 0
        self.on_t_cnt = 0
        self.off_t_cnt_contam = 0
        self.off_t_cnt_unknown = 0

        with ExitStack() as estack:
            # Create the context managers for all output files.
            report_fout = estack.enter_context(open(report_fpath, 'a'))
            on_t_fout = estack.enter_context(open(on_t_fpath, 'w'))
            off_t_fout = estack.enter_context(open(off_t_fpath, 'w'))

            self.on_t_writer = SeqWriter(on_t_fout, testSeqFileFormat(seqfile))
            self.off_t_writer = SeqWriter(off_t_fout, testSeqFileFormat(seqfile))

            seq_cnt = 0

            # Filter the reads.
            for seq_id, seq, quals in seqFileReader(seqfile):
                # The BLAST programs only take the first "word" of the sequence
                # identifier line as the sequence ID.
                blast_seq_id = seq_id.split(' ')[0]

                self.queries.append((blast_seq_id, seq_id, seq, quals))

                self.seqdb.query(seq_id, seq)

                seq_cnt += 1

            self.seqdb.inputComplete()
            self._cleanupQueriesQueue()

            if seq_cnt != self.processed_cnt:
                raise Exception(
                    'The number of sequences read ({0}) does not match the '
                    'number of sequences processed ({1}).'.format(
                        seq_cnt, self.processed_cnt
                    )
                )

            # Update the client with the final number of sequences processed.
            if (self.obs is not None):
                self.obs(self.processed_cnt)

            t_elapsed = time.perf_counter() - t_start
            t_hrs = int(t_elapsed / 3600.0)
            t_min = int((t_elapsed - (t_hrs * 3600)) / 60.0)
            t_sec = t_elapsed - (t_hrs * 3600) - (t_min * 60)

            # Generate the summary report.
            off_t_cnt = self.off_t_cnt_contam + self.off_t_cnt_unknown
            report_fout.write('{0}:\n'.format(seqfile))
            report_fout.write(
                '  Total processing time: {0:,}:{1}:{2} (h:m:s)\n'.format(
                    t_hrs, t_min, round(t_sec, 2)
                )
            )
            report_fout.write('  Total sequences: {0:,}\n'.format(seq_cnt))
            report_fout.write('  Total on target: {0:,} ({1}%)\n'.format(
                self.on_t_cnt, round((self.on_t_cnt / seq_cnt) * 100, 1)
            ))
            report_fout.write('  Total off target: {0:,} ({1}%)\n'.format(
                off_t_cnt, round((off_t_cnt / seq_cnt) * 100, 1)
            ))
            report_fout.write('    Known contaminant: {0:,} ({1}%)\n'.format(
                self.off_t_cnt_contam,
                round((self.off_t_cnt_contam / seq_cnt) * 100, 1)
            ))
            report_fout.write('    Unknown contaminant: {0:,} ({1}%)\n\n'.format(
                self.off_t_cnt_unknown,
                round((self.off_t_cnt_unknown / seq_cnt) * 100, 1)
            ))

