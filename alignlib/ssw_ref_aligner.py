# Copyright (C) 2020 Brian J. Stucky
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import ctypes as ct
from collections import namedtuple
import os.path
from .ka_stats import calcKAStats
from .ssw_lib import CSsw


# Define a struct-like datatype for storing alignment results.
AlignmentResult = namedtuple(
    'AlignmentResult',
    [
        # Optimal alignment score.
        'score',
        # Suboptimal alignment score.
        'score2',
        # 0-based best alignment beginning position on reference;
        # ref_begin1 = -1 when the best alignment beginning position is not
        # available
        'ref_begin',
        # 0-based best alignment ending position on reference
        'ref_end',
        # 0-based best alignment beginning position on query; read_begin1 = -1
        # when the best alignment beginning position is not available
        'q_begin',
        # 0-based best alignment ending position on query.
        'q_end',
        # 0-based sub-optimal alignment ending position on query.
        'ref_end_2',
        # Length of CIGAR string; == 0 when the best alignment path is not
        # available.
        'cigar_len',
        # CIGAR array (as a list).
        'cigar_arr',
        # The reference sequence for the alignment.
        'ref_seq',
        # Whether the alignment is for the reverse complement of the reference.
        'is_ref_rc',
        # Karlin-Altschul alignment statistics.
        'ka_stats'
    ]
)

# Define a struct-like datatype for storing reference sequence information.
RefSequence = namedtuple(
    'ReferenceSequence',
    [
        # The sequence ID.
        'id',
        # The sequence string.
        'seq',
        # The reverse complement of the sequence.
        'rc_seq',
        # The integer array representation of the sequence.
        'seq_int',
        # The integer array representation of the reverse complement.
        'rc_seq_int'
    ]
)

# Define all accepted nucleotide codes and their reverse complements.
nuc_codes = ('A', 'C', 'G', 'T', 'N')
rc_lookup = {
    'a': 'T', 't': 'A', 'g': 'C', 'c': 'G',
    'w': 'W', 's': 'S', 'm': 'K', 'k': 'M', 'r': 'Y', 'y': 'R',
    'b': 'V', 'd': 'H', 'h': 'D', 'v': 'B', 'n': 'N',
    'A': 'T', 'T': 'A', 'G': 'C', 'C': 'G',
    'W': 'W', 'S': 'S', 'M': 'K', 'K': 'M', 'R': 'Y', 'Y': 'R',
    'B': 'V', 'D': 'H', 'H': 'D', 'V': 'B', 'N': 'N'
} 

# Create the nucleotide-to-int lookup table.
nc_to_int = {}

for i, nc in enumerate(nuc_codes):
    nc_to_int[nc] = i
    nc_to_int[nc.lower()] = i


class SSWRefAligner:
    """
    Implements local alignment of query sequences to a "database" of one or
    more reference sequences.
    """
    def __init__(self):
        self.ssw = CSsw(os.path.dirname(os.path.realpath(__file__)))

        # Create the scoring matrix.  Use the same default values that blastn
        # uses.
        self.score_mat = self._buildScoringMatrix(nuc_codes, 1, -2)

        # A list of RefSequence objects.
        self.ref_seqs = []

        # The total length of all reference sequences.
        self.m = 0

    def _seqToInt(self, seq, nc_to_int):
        """
        Translates a nucleotide code sequence into an array of 8-bit signed
        integers.

        seq: A sequence of nucleotide character codes.
        nc_to_int: A nucleotide code-to-integer lookup dictionary.
        """
        num_decl = len(seq) * ct.c_int8
        num = num_decl()

        for i, nc in enumerate(seq):
            try:
                num[i] = nc_to_int[nc]
            except KeyError:
                print('WARNING: Nucleotide code "{0}" was not recognized.'.format(
                    nc
                ))
                num[i] = nc_to_int['N']

        return num

    def _buildScoringMatrix(self, nuc_codes, match_val, mismatch_val):
        """
        Creates an alignment scoring matrix given the set of nucleotide codes
        and match/mismatch values.  Matches with an 'N' are ambiguous and
        always assigned a score of 0.  The matrix is returned as an array of
        signed 8-bit integers in row-major order.
        """
        nc_cnt = len(nuc_codes)
        s_mat = [0 for i in range(nc_cnt**2)]

        for i in range(nc_cnt):
            for j in range(nc_cnt):
                s_mat_i = i*nc_cnt + j
                if nuc_codes[i] == 'N' or nuc_codes[j] == 'N':
                    s_mat[s_mat_i] = 0
                elif nuc_codes[i] == nuc_codes[j]:
                    # Score for a nucleotide match.
                    s_mat[s_mat_i] = match_val
                else:
                    # Score for a nucleotide mismatch.
                    s_mat[s_mat_i] = mismatch_val

        # Convert the scoring matrix to an array of 8-bit signed integers.
        mat = (len(s_mat) * ct.c_int8) ()
        mat[:] = s_mat

        return mat

    def _printScoringMatrix(self, nuc_codes, s_mat):
        """
        A generic method to pretty-print a scoring matrix stored as a sequence
        of integers in row-major order.

        nuc_codes: A sequence of accepted nucleotide codes, in the same order
            as the columns/rows of the scoring matrix.
        s_mat: A sequence of integers.
        """
        nc_cnt = len(nuc_codes)

        colnames_format_str = ' '.join(
            ['{{{0}:>3}}'.format(i) for i in range(nc_cnt)]
        )
        colnames_format_str = '  ' + colnames_format_str

        row_format_str = ','.join(
            ['{{{0}:>3}}'.format(i+1) for i in range(nc_cnt)]
        )
        row_format_str = '{0}:' + row_format_str

        print(colnames_format_str.format(*nuc_codes))
        print('   --' + '----' * (nc_cnt - 1))

        for i in range(nc_cnt):
            s_mat_i = i * nc_cnt
            row = s_mat[s_mat_i:s_mat_i + nc_cnt]
            print(row_format_str.format(nuc_codes[i], *row))

    def _doAlignment(self, q_profile, ref_seq, n, m, mask_len, use_rc):
        """
        q_profile: The profile of the query sequence.
        ref_seq_int: A RefSequence object.
        n: The length of the query sequence.
        m: The length of the reference sequence.
        mask_len: The mask length to use.
        use_rc: Whether to align to the reverse complement.
        """
        if use_rc:
            rs_int = ref_seq.rc_seq_int
        else:
            rs_int = ref_seq.seq_int

        res = self.ssw.ssw_align(
            q_profile, rs_int, ct.c_int32(len(ref_seq.seq)),
            # The gap opening and extension penalties, respectively.  By
            # default, blastn uses linear gap penalties with a penalty of 0 for
            # gap opening and 2.5 for gap extension (with scoring matrix values
            # of 1 and -2).
            #
            # Although the SSW module supports affine gap penalties, I have
            # found some oddities with the implementation.  First, if the
            # extension penalty is greater than the opening penalty, it appears
            # that the "opening penalty" is assessed against each gap position
            # and the extension penalty is ignored.  Second, if the gap
            # extension penalty is equal to or greater than the gap opening
            # penalty, SSW segfaults on some sequences.  SSW also only supports
            # integer-valued gap penalties, which is by design.
            #
            # Thus, to account for all of the above, we use penalties of 3 for
            # gap opening and 2 for gap extension, which is as close as we can
            # get to the blastn defaults.  This should not impact the K-A
            # statistical results, because the default K-A parameters used by
            # blastn are actually for the case where the gap penalties are 0
            # (see the parameter tables in blast_stat.c in the BLAST sources).
            3, 2,
            2, 0, 0, mask_len
        )

        align_res = AlignmentResult(
            score = res.contents.nScore,
            score2 = res.contents.nScore2,
            ref_begin = res.contents.nRefBeg,
            ref_end = res.contents.nRefEnd,
            q_begin = res.contents.nQryBeg,
            q_end = res.contents.nQryEnd,
            ref_end_2 = res.contents.nRefEnd2,
            cigar_len = res.contents.nCigarLen,
            cigar_arr = [
                res.contents.sCigar[i] for i in range(res.contents.nCigarLen)
            ],
            ref_seq = ref_seq,
            is_ref_rc = use_rc,
            ka_stats = calcKAStats(res.contents.nScore, n, m)
        )

        self.ssw.align_destroy(res)

        return align_res

    def _buildPath(self, q, r, nQryBeg, nRefBeg, lCigar):
        """
        Taken directly from pyssw.py, with minor formatting changes.

        Build CIGAR string and align path based on CIGAR array returned by
        ssw_align
        @param  q   query sequence
        @param  r   reference sequence
        @param  nQryBeg   begin position of query sequence
        @param  nRefBeg   begin position of reference sequence
        @param  lCigar   cigar array
        """
        sCigarInfo = 'MIDNSHP=X'
        sCigar = ''
        sQ = ''
        sA = ''
        sR = ''
        nQOff = nQryBeg
        nROff = nRefBeg
        for x in lCigar:
            n = x >> 4
            m = x & 15
            if m > 8:
                c = 'M'
            else:
                c = sCigarInfo[m]
            sCigar += str(n) + c

            if c == 'M':
                sQ += q[nQOff : nQOff+n]
                sA += ''.join(
                    ['|' if q[nQOff+j] == r[nROff+j] else '*' for j in range(n)]
                )
                sR += r[nROff : nROff+n]
                nQOff += n
                nROff += n
            elif c == 'I':
                sQ += q[nQOff : nQOff+n]
                sA += ' ' * n
                sR += '-' * n
                nQOff += n
            elif c == 'D':
                sQ += '-' * n
                sA += ' ' * n
                sR += r[nROff : nROff+n]
                nROff += n

        return sCigar, sQ, sA, sR

    def getScoringMatrix(self):
        return self.score_mat

    def printScoringMatrix(self):
        """
        Pretty-prints the scoring matrix.
        """
        self._printScoringMatrix(nuc_codes, self.score_mat)

    def printAlignment(self, out, res, query_seq_id, query_seq):
        """
        out: The output stream.
        res: The alignment results as returned by ssw_align().
        query_seq_id: The ID of the query sequence.
        query_seq: The query sequence.
        """
        if res.is_ref_rc:
            r_seq = res.ref_seq.rc_seq
        else:
            r_seq = res.ref_seq.seq

        sCigar, sQ, sA, sR = self._buildPath(
            query_seq, r_seq, res.q_begin, res.ref_begin, res.cigar_arr
        )

        out.write('Query sequence: {0} ({1:,} bp)\n'.format(
            query_seq_id, len(query_seq)
        ))
        out.write('Reference sequence: {0} ({1:,} bp)\n\n'.format(
            res.ref_seq.id, len(res.ref_seq.seq)
        ))
        out.write('Reference sequence begin and end: {0}, {1}\n'.format(
            res.ref_begin + 1, res.ref_end + 1
        ))
        out.write('Query sequence begin and end: {0}, {1}\n'.format(
            res.q_begin + 1, res.q_end + 1
        ))

        if res.cigar_len > 0:
            n1 = 1 + res.ref_begin
            n2 = min(60, len(sR)) + res.ref_begin - sR.count('-',0,60)
            n3 = 1 + res.q_begin
            n4 = min(60, len(sQ)) + res.q_begin - sQ.count('-',0,60)
            for i in range(0, len(sQ), 60):
                out.write('Target:{:>8}\t{}\t{}\n'.format(n1, sR[i:i+60], n2))
                n1 = n2 + 1
                n2 = n2 + min(60,len(sR)-i-60) - sR.count('-',i+60,i+120)

                out.write('{: ^15}\t{}\n'.format('', sA[i:i+60]))

                out.write('Query:{:>9}\t{}\t{}\n\n'.format(n3, sQ[i:i+60], n4))
                n3 = n4 + 1
                n4 = n4 + min(60,len(sQ)-i-60) - sQ.count('-',i+60,i+120)

        out.write('Optimal alignment score: {0:,}\n'.format(res.score))
        out.write('Effective search space size: {0:,}\n'.format(
            res.ka_stats.esss
        ))
        out.write('E-value: {0}\n\n'.format(res.ka_stats.E))

    def addReference(self, ref_seq_id, ref_seq):
        if len(ref_seq) == 0:
            return

        rc_ref_seq = ''.join([rc_lookup[nc] for nc in ref_seq[::-1]])

        # Generate the integer representation of the reference sequence and its
        # reverse complement.
        ref_seq_int = self._seqToInt(ref_seq, nc_to_int)
        rc_ref_seq_int = self._seqToInt(rc_ref_seq, nc_to_int)
 
        rs = RefSequence(
            id=ref_seq_id, seq=ref_seq, rc_seq=rc_ref_seq, seq_int=ref_seq_int,
            rc_seq_int=rc_ref_seq_int
        )

        self.ref_seqs.append(rs)

        self.m += len(ref_seq)

    def getRefSeqCount(self):
        return len(self.ref_seqs)

    def querySearch(self, q_seq):
        """
        Does a local alignment of a query sequence to each reference sequence,
        checking both the original reference sequence and its reverse
        complement.

        q_seq: The query sequence as a string.
        ref_seq_int: The reference sequence as an array of 8-bit signed
            integers.
        rec_ref_seq_int: The reverse complement of ref_seq_int.
        """
        if len(self.ref_seqs) == 0:
            raise Exception('No reference sequences are available.')

        # Build the query profile.
        q_int_seq = self._seqToInt(q_seq, nc_to_int)
        q_profile = self.ssw.ssw_init(
            q_int_seq, ct.c_int32(len(q_seq)), self.score_mat, len(nuc_codes),
            2
        )

        # Set the mask length.
        if len(q_seq) > 30:
            mask_len = len(q_seq) // 2
        else:
            mask_len = 15

        # Align to each reference sequence and return the alignment with the
        # highest score (which is also guaranteed to have the lowest E-value
        # since the length of the query sequence and database size are both
        # constant for the comparisons).
        best = None
        for rs in self.ref_seqs:
            res1 = self._doAlignment(
                q_profile, rs, len(q_seq), self.m, mask_len, False
            )
            res2 = self._doAlignment(
                q_profile, rs, len(q_seq), self.m, mask_len, True
            )

            if best is None:
                best = res1

            if res1.score > best.score:
                best = res1

            if res2.score > best.score:
                best = res2

        return best

